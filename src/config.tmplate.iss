; =============================================================================
; DreamSDK Setup - Inno Setup Script
; =============================================================================

; Installer mode
#define InstallerMode DEBUG
#define SourceMode RELEASE
#define CompressionMode COMPRESSION_DISABLED
#define DigitalSignatureMode SIGNATURE_DISABLED

; Installer versions
#define AppVersion "R3"
#define PackageVersion "3.0.4.2206"
#define ProductVersion "3.0.4.2206"

; Copyright
#define MyAppCopyright "� Copyleft 2018-2022"
